# YOSS Test 

---

# React app

Develop a small React app which includes 2 main routes:
- the first one will display some iTunes data ([iTunes Search API](https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/))
- the other one will display some weather data ([Weather API](https://www.metaweather.com/api/))

You can use the routing solution of your choice, we suggest [react-router-dom](https://github.com/ReactTraining/react-router/tree/master/packages/react-router-dom).

## Homepage route `/`

Includes a top navbar with a link to each route.

## iTunes route `/itunes`

This page is composed of a search input and the results section.

The results section is composed of two lists (Artists and Songs), each list provides links to a details view.

Each detail view displays the most useful data returned by the API, and if possible a picture.

## Weather route `/weather`

This page is composed of a search input and the results section.

The results section is a grid composed of the most useful data returned by the API, if possible with icons.
