export default {
  name: "apiItunes",
  host: "https://itunes.apple.com/search?",
  query: {
    media: "music",
    entity: 'song',
    attribute: 'songTerm',
    term: null,
    //limit: 200
  }
};
