import React from "react";
import { observer } from "mobx-react";
import Search from "../Components/search";
import ApiItunes from "../Api/Itunes";
import "./Itunes.css";

@observer
class Itunes extends React.Component {
  handleChange = event => {
    const value = event.target.value;
    ApiItunes.query.term = value;
    this.props.store.searchValue = value;
    this.props.store.selectResults(ApiItunes.host, ApiItunes.query);
  };

  renderCollection(collection) {
    return (
      <React.Fragment key={collection.collectionId}>
        <div>Name collection: {collection.collectionName}</div>
        <div>
          {collection.tracks.map(track => {
            return <div key={track.trackId}>Track name: {track.trackName}</div>;
          })}
        </div>
        <hr />
      </React.Fragment>
    );
  }

  renderResult(result) {
    return (
      <React.Fragment key={result.artistId}>
        <div className="card">
          <h2 className="card-header">Artist Name: {result.artistName}</h2>
          <div className="card-body">
            {result.collections.map(collection => {
              return this.renderCollection(collection);
            })}
          </div>
        </div>
      </React.Fragment>
    );
  }

  renderResults(results) {
    if (results.length > 0) {
      return results.map(this.renderResult.bind(this));
    } else {
      return <p>No result yet</p>;
    }
  }

  render() {
    const results = this.props.store.results;

    return (
      <>
        <div className="d-flex flex-column align-items-center">
          <h1>Itunes API Search</h1>
          <p className="font-weight-light">Find a song</p>
          <div className="itunes-input">
            <Search handleChange={this.handleChange} />
          </div>
          <div className="d-flex justify-content-center itunes-subtitles">
            <p>Total Artists: {this.props.store.results.length}</p>
          </div>
        </div>

        <div className="d-flex flex-column justify-content-end">
          {this.renderResults(results)}
        </div>
      </>
    );
  }
}

export default Itunes;
