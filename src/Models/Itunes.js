import { observable, action } from "mobx";
import queryString from "query-string";

export default class Itunes {
  @observable searchValue = "input vide";
  @observable results = [];

  constructor() {
    this.lastUrl = null;
  }

  normalize(results) {
    let store = [];

    const selectArtistById = id => {
      return store.find(element => {
        return element.artistId === id;
      });
    };

    const selectCollectionById = (artiste, id) => {
      return artiste.collections.find(element => {
        return element.collectionId === id;
      });
    };

    results.forEach(result => {
      const {
        artistId,
        artistName,
        collectionId,
        collectionName,
        trackName,
        trackId
      } = result;

      const track = { trackName, trackId };
      const tracks = [{ trackName, trackId }];
      const collection = { collectionId, collectionName, tracks };
      const collections = [{ collectionId, collectionName, tracks }];
      const artist = { artistId, artistName };

      if (selectArtistById(artistId) === undefined) {
        store.push({ ...artist, collections });
      } else {
        const storeArtiste = selectArtistById(artistId);
        const storeCollection = selectCollectionById(
          storeArtiste,
          collectionId
        );

        if (storeCollection === undefined) {
          storeArtiste.collections.push(collection);
        } else {
          storeCollection.tracks.push(track);
        }
      }
    });

    return store
  }

  @action
  selectResults(host, query) {
    const url = host.concat(queryString.stringify(query));
    this.lastUrl = url;

    fetch(url)
      .then(res => res.json())
      .then(
        res => {
          if (this.lastUrl === url) {
            this.results = this.normalize(res.results);
          }
        },
        error => {
          console.log("api", error);
        }
      );
  }
}
