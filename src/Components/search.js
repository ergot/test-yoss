import React from "react";
import PropTypes from "prop-types";

const search = props => {
  return <input type="text" onChange={props.handleChange} />;
};

search.propTypes = {
  handleChange: PropTypes.func.isRequired
};

export default search;
