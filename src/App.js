import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Itunes from "./Containers/Itunes";
import ItunesModel from "./Models/Itunes";

const itunesModel = new ItunesModel()

const routes = [
  {
    path: "/",
    exact: true,
    main: () => <Home />
  },
  {
    path: "/itunes",
    main: () => <Itunes  store={itunesModel}/>
  },
  {
    path: "/weather",
    main: () => <Weather />
  }
];

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <nav className="navbar navbar-dark bg-dark">
            <Link to="/">Home</Link>
            <Link to="/itunes">Itunes</Link>
            <Link to="/weather">weather</Link>
          </nav>

          <div className='container'>
            {routes.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
            ))}
          </div>
        </div>
      </Router>
    );
  }
}

function Home() {
  return (
    <div>
      <h2>Home</h2>
    </div>
  );
}

function Weather() {
  return (
    <div>
      <h2>Weather</h2>
    </div>
  );
}
export default App;
